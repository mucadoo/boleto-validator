const Joi = require('joi');

const joiExtendedString = Joi.extend({
  base: Joi.string(),
  name: 'string',
  language: {
    one: 'Please enter a valid number. Barcodes must ALWAYS be 44 numeric characters long. Typeable lines can have 46 (credit card slips), 47 (bank slips/collection slips) or 48 (agreement/collection accounts) numeric characters. Any non-numeric characters will be disregarded.',
    two: 'This type of bank slip must have a 44-character numeric barcode. Or typeable line of 48 numeric characters.',
    three: 'Check digit validation failed. Are you sure you entered the correct number?'
  },
  rules: [{
    name: 'validBoletoLineCode',
    validate(params, value, state, options) {
      if (value.length == 36) {
        value = value + '00000000000';
      } else if (value.length == 46) {
        value = value + '0';
      }
    
      if (value.length != 44 && value.valida != 46 && value.length != 47 && value.length != 48) {
        return this.createError('string.one', { v: value }, state, options);
      } else if (value.substr(0, 1) == '8' && value.length == 46 && value.length == 47) {
        return this.createError('string.two', { v: value }, state, options);
      } else if (!validateLineCode(value)) {
        return this.createError('string.three', { v: value }, state, options);
      }
    
      return value;
    }
  }]
});

function validateLineCode(lineCode) {

    let result;

    //se for cc ou bk
    if (lineCode.substr(-14) == '00000000000000' || lineCode.substr(5, 14) == '00000000000000' || lineCode.substr(0, 1) != '8') {
        const block1 = lineCode.substr(0, 9) + mod10calc(lineCode.substr(0, 9));
        const block2 = lineCode.substr(10, 10) + mod10calc(lineCode.substr(10, 10));
        const block3 = lineCode.substr(21, 10) + mod10calc(lineCode.substr(21, 10));
        const block4 = lineCode.substr(32, 1);
        const block5 = lineCode.substr(33);
        result = (block1 + block2 + block3 + block4 + block5).toString();
    } else {
        let mod;
        let block1;
        let block2;
        let block3;
        let block4;

        //2,1 = ref
        if(lineCode.substr(2, 1) == "6" || lineCode.substr(2, 1) == "7"){
            mod = 10;
        } else if(lineCode.substr(2, 1) == "8" || lineCode.substr(2, 1) == "9"){
            mod = 11;
        }

        if (mod == 10) {
            block1 = lineCode.substr(0, 11) + mod10calc(lineCode.substr(0, 11));
            block2 = lineCode.substr(12, 11) + mod10calc(lineCode.substr(12, 11));
            block3 = lineCode.substr(24, 11) + mod10calc(lineCode.substr(24, 11));
            block4 = lineCode.substr(36, 11) + mod10calc(lineCode.substr(36, 11));
        } else if (mod == 11) {
            block1 = lineCode.substr(0, 11);
            block2 = lineCode.substr(12, 11);
            block3 = lineCode.substr(24, 11);
            block4 = lineCode.substr(36, 11);

            let dv1 = parseInt(lineCode.substr(11, 1));
            let dv2 = parseInt(lineCode.substr(23, 1));
            let dv3 = parseInt(lineCode.substr(35, 1));
            let dv4 = parseInt(lineCode.substr(47, 1));

            let valid = (mod11calc(block1) == dv1 &&
                mod11calc(block2) == dv2 &&
                mod11calc(block3) == dv3 &&
                mod11calc(block4) == dv4)

            return valid;
        }

        result = block1 + block2 + block3 + block4;
    }

    return lineCode === result;
}

function mod10calc(number) {
    number = number.replace(/\D/g, '');
    var i;
    var mult = 2;
    var sum = 0;
    var s = '';

    for (i = number.length - 1; i >= 0; i--) {
        s = (mult * parseInt(number.charAt(i))) + s;
        if (--mult < 1) {
            mult = 2;
        }
    }
    for (i = 0; i < s.length; i++) {
        sum = sum + parseInt(s.charAt(i));
    }
    sum = sum % 10;
    if (sum != 0) {
        sum = 10 - sum;
    }
    return sum;
}

function mod11calc(x) {
    let sequence = [4, 3, 2, 9, 8, 7, 6, 5];
    let digit = 0;
    let j = 0;
    let DAC = 0;

    for (var i = 0; i < x.length; i++) {
        let mult = sequence[j];
        j++;
        j %= sequence.length;
        digit += mult * parseInt(x.charAt(i));
    }

    DAC = digit % 11;

    if (DAC == 0 || DAC == 1)
        return 0;
    if (DAC == 10)
        return 1;

    return (11 - DAC);

}

module.exports = joiExtendedString;