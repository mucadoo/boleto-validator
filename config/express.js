const express = require('express');
const logger = require('morgan');
const httpStatus = require('http-status');
const expressValidation = require('express-validation');
const routes = require('./routes');
const APIError = require('../helpers/APIError');

const app = express();

app.use(logger('dev'));

app.use('/', routes);

app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    console.log(err);
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});

app.use((err, req, res, next) =>
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    //stack: err.stack
  })
);

module.exports = app;
