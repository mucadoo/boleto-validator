const Joi = require('joi');

require('dotenv').config();

const envVarsSchema = Joi.object({
  PORT: Joi.number()
    .default(4040)
}).unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  port: envVars.PORT
};

module.exports = config;
