const Joi = require('../helpers/validationUtils');

module.exports = {

  // GET /api/boleto/:lineCode
  getBoletoInfo: {
    params: {
      lineCode: Joi.string().validBoletoLineCode()
    }
  }

};
