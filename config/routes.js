const express = require('express');
const boletoRoutes = require('../server/boleto/boleto.route');

const router = express.Router();

router.use('/boleto', boletoRoutes);

module.exports = router;
