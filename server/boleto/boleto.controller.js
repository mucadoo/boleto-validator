const Boleto = require('./boleto.model');

function load(req, res, next, lineCode) {
  req.boleto = new Boleto(lineCode);
  return next();
}

function get(req, res) {
  return res.json(req.boleto.toAPIObj());
}

module.exports = { load, get };
