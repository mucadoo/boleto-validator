function Boleto(lineCode) {
  if (lineCode.length == 36) {
    lineCode = lineCode + '00000000000';
  } else if (lineCode.length == 46) {
    lineCode = lineCode + '0';
  }
  this.lineCode = lineCode;
  this.type  = null;
  this.barCode  = null;
  this.dueDate  = null;
  this.value  = null;
  this.detectType();
  this.lineCode2BarCode();
  this.identifyDueDate();
  this.identifyValue();
}

Boleto.prototype.getLineCode = function() {
  return this.lineCode;
}

Boleto.prototype.setLineCode = function(lineCode) {
  this.lineCode = lineCode;
}

Boleto.prototype.getType = function() {
  return this.type;
}

Boleto.prototype.setType = function(type) {
  this.type = type;
}

Boleto.prototype.getBarCode = function() {
  return this.barCode;
}

Boleto.prototype.setBarCode = function(barCode) {
  this.barCode = barCode;
}

Boleto.prototype.getDueDate = function() {
  return this.dueDate;
}

Boleto.prototype.setDueDate = function(dueDate) {
  this.dueDate = dueDate;
}

Boleto.prototype.getValue = function() {
  return this.value;
}

Boleto.prototype.setValue = function(value) {
  this.value = value;
}

Boleto.prototype.detectType = function() {
  let lineCode = this.getLineCode();

  if (lineCode.substr(-14) == '00000000000000' || lineCode.substr(5, 14) == '00000000000000') {
      this.setType('CREDIT_CARD');
  } else if (lineCode.substr(0, 1) == '8') {
      if (lineCode.substr(1, 1) == '1') {
          this.setType('CITY_HALL');
      } else if (lineCode.substr(1, 1) == '2') {
          this.setType('SANITATION');
      } else if (lineCode.substr(1, 1) == '3') {
          this.setType('ELECTRIC_POWER_AND_GAS');
      } else if (lineCode.substr(1, 1) == '4') {
          this.setType('TELECOMMUNICATIONS');
      } else if (lineCode.substr(1, 1) == '5') {
          this.setType('GOVERNMENT_AGENCIES');
      } else if (lineCode.substr(1, 1) == '6' || codigo.substr(1, 1) == '9') {
          this.setType('OTHERS');
      } else if (lineCode.substr(1, 1) == '7') {
          this.setType('TRAFFIC_FEES');
      }
  } else {
    this.setType('BANK');
  }
}

Boleto.prototype.lineCode2BarCode = function() {
  let lineCode = this.getLineCode();
  let type = this.getType();

  let result = '';

  if (type == 'BANK' || type == 'CREDIT_CARD') {
    result = lineCode.substr(0, 4) +
          lineCode.substr(32, 1) +
          lineCode.substr(33, 14) +
          lineCode.substr(4, 5) +
          lineCode.substr(10, 10) +
          lineCode.substr(21, 10);
  } else {

      lineCode = lineCode.split('');
      lineCode.splice(11, 1);
      lineCode.splice(22, 1);
      lineCode.splice(33, 1);
      lineCode.splice(44, 1);
      lineCode = lineCode.join('');

      result = lineCode;
  }

  this.setBarCode(result);
}

Boleto.prototype.identifyDueDate = function() {
  
  let moment = require('moment-timezone');
  let lineCode = this.getLineCode();
  let type = this.getType();

  if (type == 'BANK' || type == 'CREDIT_CARD') {
    let dateCoefficient = lineCode.substr(33, 4);
    let baseDate = moment.tz("1997-10-07 20:54:59.000Z", "UTC");
    baseDate.add(Number(dateCoefficient), 'days');
    this.setDueDate(baseDate.format('YYYY-MM-DD'));
  } else {
    this.setDueDate("");
  }

}

Boleto.prototype.identifyValue = function() {

  let type = this.getType();
  let lineCode = this.getLineCode();

  let value = '';
  let finalValue;
  let isEffectiveValue = lineCode.substr(2, 1) == '6' || lineCode.substr(2, 1) == '8';

  if (type == 'BANK' || type == 'CREDIT_CARD') {
    value = lineCode.substr(37);
    finalValue = value.substr(0, 8) + '.' + value.substr(8, 2);
  } else if (isEffectiveValue) {
    value = lineCode.substr(4, 14);
    value = lineCode.split('');
    value.splice(11, 1);
    value = value.join('');
    value = value.substr(4, 11);
    finalValue = value.substr(0, 9) + '.' + value.substr(9, 2);
  }

  if(finalValue){
    let char = finalValue.substr(1, 1);
    while (char === '0') {
      finalValue = [
        finalValue.slice(0, 0),
        ''.substr(0, 1),
        ''.slice(1),
        finalValue.slice(0 + 1)
      ].join('');
      char = finalValue.substr(1, 1);
    }
    this.setValue(parseFloat(finalValue));
  } else {
    this.setValue(0);
  }

}

Boleto.prototype.toAPIObj = function() {
  console.log(this);
  return {
    barCode: this.getBarCode(),
    amount: this.getValue(),
    expirationDate: this.getDueDate()
  }
}

module.exports = Boleto;
