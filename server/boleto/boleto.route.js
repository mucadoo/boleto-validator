const express = require('express');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const boletoCtrl = require('./boleto.controller');

const router = express.Router();

router.route('/')

router.route('/:lineCode')
  /** GET /api/boleto/:lineCode - Get boleto */
  .get(validate(paramValidation.getBoletoInfo), boletoCtrl.get)

/** Load boleto when API with lineCode route parameter is hit */
router.param('lineCode', boletoCtrl.load);

module.exports = router;
