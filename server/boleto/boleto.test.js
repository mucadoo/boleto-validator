const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const chai = require('chai');
const expect = chai.expect;
const app = require('../../index');

chai.config.includeStack = true;

describe('## Boleto API', () => {
  let boleto_internet = {
    lineCode: "846800000016199900820895993388570117422412616993",
    barCode: "84680000001199900820899933885701142241261699",
    amount: 119.99,
    expirationDate: ""
  };

  let boleto_banco = {
    lineCode: "26090356359263619492710100000008289140000173133",
    barCode: "26092891400001731330356392636194921010000000",
    amount: 1731.33,
    expirationDate: "2022-03-04"
  };

  let error = {
    lineCode: "260903563",
    message: '"lineCode" Please enter a valid number. Barcodes must ALWAYS be 44 numeric characters long. Typeable lines can have 46 (credit card slips), 47 (bank slips/collection slips) or 48 (agreement/collection accounts) numeric characters. Any non-numeric characters will be disregarded.'
  }

  describe('# GET /boleto/:lineCode', () => {
    it('should get telecommunication boleto details', (done) => {
      request(app)
        .get(`/boleto/${boleto_internet.lineCode}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.barCode).to.equal(boleto_internet.barCode);
          expect(res.body.amount).to.equal(boleto_internet.amount);
          expect(res.body.expirationDate).to.equal(boleto_internet.expirationDate);
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /boleto/:lineCode', () => {
    it('should get bank boleto details', (done) => {
      request(app)
        .get(`/boleto/${boleto_banco.lineCode}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.barCode).to.equal(boleto_banco.barCode);
          expect(res.body.amount).to.equal(boleto_banco.amount);
          expect(res.body.expirationDate).to.equal(boleto_banco.expirationDate);
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /boleto/:lineCode', () => {
    it('should get an error message', (done) => {
      request(app)
        .get(`/boleto/${error.lineCode}`)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body.message).to.equal(error.message);
          done();
        })
        .catch(done);
    });
  });

});
